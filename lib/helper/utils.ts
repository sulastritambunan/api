const FormData = require('form-data');
const jwtDecode = require('jwt-decode');
const reporter = require('wdio-reportportal-reporter');
const { MAX_PERCENTAGE_REFUND } = require('./constants');

export const constructPayloadFormData = async (objectInput:any) => {
    const formBuilder = new FormData();

    // begin to construct
    Object.keys(objectInput).forEach((keyObject) => {
        formBuilder.append(keyObject, objectInput[keyObject]);
    });

    return formBuilder;
};

export const generatePINPotentialInstallment = () => {
    const today = new Date();
    const pinCode = today.getFullYear() + today.getDate() + (today.getMonth() + 1);

    return pinCode;
};

export const generatePlatNomor = () => {
    const nopol = Math.floor(Math.random() * (9999 - 83 + 1)) + 83;
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const charactersLength = characters.length;
    let hurufBelakang = '';
    for (let i = 0; i < 3; i++) {
        hurufBelakang += characters.charAt(
            Math.floor(Math.random() * charactersLength),
        );
    }
    return `B${nopol}${hurufBelakang}`;
};

export const generateIncentiveName = () => {
    const today = new Date();
    const options = {
        timeZone: 'Asia/Jakarta',
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    return `SDET Automation Incentive Test ${today.toLocaleDateString('id-ID', options)}`;
};

export const calculateRefund = (refundAmt:string) => {
    const tempVal = parseInt(refundAmt, 10);
    return Math.floor(tempVal - MAX_PERCENTAGE_REFUND * tempVal);
};

export const sendReportPortalLog = (res:any) => {
    try {
        reporter.sendLog('INFO', `Request Data: ${res.config.data}`);
        reporter.sendLog('INFO', `Request URL: ${res.config.url}`);
        reporter.sendLog('INFO', `Request Headers: ${JSON.stringify(res.config.headers, null, 4)}`);
        reporter.sendLog('INFO', `Request Method: ${res.config.method}`);
        reporter.sendLog('INFO', `Response Body: ${JSON.stringify(res.data, null, 4)}`);
        reporter.sendLog('INFO', `Response Status: ${res.status}`);
    } catch (e) {
        console.log(e);
    }
};

export const decodeJwtToken = (token:string) => {
    const decodeToken = jwtDecode(token);
    return decodeToken;
};
