/* eslint-disable no-undef */
export const delay = async ( timeout: string ) => {
    await browser.pause(timeout);
};
