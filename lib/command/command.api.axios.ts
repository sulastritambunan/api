/* eslint-disable no-await-in-loop */
import * as dotenv from 'dotenv'
dotenv.config();
import axios from 'axios';
//const SlackReporter = require('../../../utils/slack/slack.reporter');

export const axiosPost = async (
    path?:string,
    data?:string,
    token?:string,
    isErrorExpected = false,
    baseURL = process.env.endpointMoladinEvo,
    type = 'json',
) => {
    try {
        const res = await axios.post(
            baseURL + path,
            data,
            {
                headers: {
                    ...((type === 'json') && { 'Content-Type': 'application/json' }),
                    ...((type === 'form') && { 'Content-Type': `multipart/form-data; boundary=${data._boundary}` }),
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return res;
    } catch (error:any) {
        if (error.response) {
            if (!isErrorExpected) {
                console.log(error.toJSON());
                await SlackReporter.sendRequestErrorMessage({
                    req: data,
                    res: error.response
                });
                console.log(
                    `Error Response: ${JSON.stringify(error.response.data)}`,
                );
            }
            return error.response;
        }
    }
};

export const axiosGet = async (
    path?:string,
    token?:string,
    isErrorExpected = false,
    baseURL = process.env.endpointMoladinEvo,
) => {
    try {
        const res = await axios.get(
            baseURL + path,
            {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return res;
    } catch (error:any) {
        if (error.response) {
            if (!isErrorExpected) {
                console.log(error.toJSON());
                await SlackReporter.sendRequestErrorMessage({
                    req: data,
                    res: error.response
                });
                console.log(
                    `Error Response: ${JSON.stringify(error.response.data)}`,
                );
            }
            return error.response;
        }
    }
};

export const axiosPut = async (
    path?:string,
    token?:string,
    data?:string,
    isErrorExpected = false,
    baseURL = process.env.endpointMoladinEvo,
    type = 'json',
) => {
    try {
        const resp = await axios.put(
            baseURL + path,
            data,
            {
                headers: {
                    ...((type === 'json') && { 'Content-Type': 'application/json' }),
                    ...((type === 'form') && { 'Content-Type': `multipart/form-data; boundary=${data._boundary}` }),
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return resp;
    } catch (error:any) {
        if (error.response) {
            if (!isErrorExpected) {
                console.log(error.toJSON());
                await SlackReporter.sendRequestErrorMessage({
                    req: data,
                    res: error.response
                });
                console.log(
                    `Error Response: ${JSON.stringify(error.response.data)}`,
                );
            }
            return error.response;
        }
    }
};
